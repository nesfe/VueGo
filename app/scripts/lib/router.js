import Vue from 'vue';
import Router from 'vue-router';
import Resource from 'vue-resource';

import Home from '../component/home.vue';
import Info from '../component/info.vue';

Vue.use(Router);
Vue.use(Resource);

let router = new Router({
    linkActiveClass: "active",
    routes: [
        {path: '/home', name: 'home', component: Home},
        {path: '/info', name: 'info', component: Info},
        {path: '*', redirect: '/home'}
    ]
});


// router.afterEach(function (to, from) {
//     if(!url[to.path]){
//         $('#Container').show();
//         $('#Sspinner').hide();
//         NProgress.done();
//     }
// });
// router.beforeEach(function (to,from,next) {
//     NProgress.start();
//     $('#Container').hide();
//     $('#Sspinner').show();
//     next(true);
// });
// Vue.http.interceptors.push(function (request, next) {
//     next(function (response) {
//         NProgress.done();
//         $('#Container').show();
//         $('#Sspinner').hide();
//     });
// });

export {router}