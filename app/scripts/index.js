import './lib/vendor.js';
import 'bootstrap';
import Vue from 'vue';
import {router} from './lib/router.js';

new Vue({
    router: router
}).$mount('#main');